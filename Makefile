WARN    := -Wall
CFLAGS  := -O2 ${WARN}
LDFLAGS :=

C_SRCS    = chkugenids.c findugendev.c
OBJ_FILES = $(C_SRCS:.c=.o)

all: chkugenids findugendev

%.o: %.c
	${CC} ${WARN} -c ${CFLAGS}  $< -o $@

chkugenids: chkugenids.o
	${CC} ${WARN} -o $@ chkugenids.o ${LDFLAGS}

findugendev: findugendev.o
	${CC} ${WARN} -o $@ findugendev.o ${LDFLAGS}

clean:
	rm -rf *.o chkugenids findugendev

mrproper: clean
	rm -rf *~
