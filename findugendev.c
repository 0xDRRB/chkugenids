/*
 * Copyright (c) 2024 Denis Bodor
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIEDi
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <regex.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include <sys/ioctl.h>
#if defined(__NetBSD__) || defined(__OpenBSD__)
#include <dev/usb/usb.h>
#else
#include <dev/usb/usb_ioctl.h>
#endif

#define SEARCHVID     1
#define SEARCHPID     2

int optverb;
int optdevusb;
int optdevusbonly;

int
match_regex(const char *string, const char *pattern, int optcase)
{
    int ret;
    regex_t reg;

    if (regcomp(&reg, pattern, REG_EXTENDED|REG_NOSUB|optcase) != 0) {
		if (optverb)
			fprintf(stderr, "Regex compilation error\n");
        return 0;
    }

    ret = regexec(&reg, string, (size_t)0, NULL, 0);

    regfree(&reg);

    if (ret == 0)
        return 1;

    return 0;
}

int
checkid(char *filename, uint16_t vid, uint16_t pid, int opt)
{
	int fd;
	char *filepath;
	struct usb_device_info devinfo;

	if ((filepath = (char *) malloc(strlen(filename)+1+5)) == NULL) {
		err(EXIT_FAILURE, "Memory allocation error");
	}

	snprintf(filepath, strlen(filename)+1+5, "/dev/%s", filename);

	if ((fd = open(filepath, O_RDONLY)) == -1) {
		if (optverb)
			warn("%s: open failed.", filename);
		if (errno == EACCES)
			warn("Opening failed.");
		free(filepath);
		return 0;
	}

	if (strstr(filename, "ugen") != NULL) {
		if (ioctl(fd, USB_GET_DEVICEINFO, &devinfo) == -1) {
			if (optverb)
				warn("%s: ioctl failed.", filename);
			close(fd);
			free(filepath);
			return 0;
		}

		if ((devinfo.udi_vendorNo == vid && opt == SEARCHVID) ||
				(devinfo.udi_productNo == pid && opt == SEARCHPID) ||
				(devinfo.udi_vendorNo == vid && devinfo.udi_productNo == pid && opt == (SEARCHVID | SEARCHPID))) {
			close(fd);
			free(filepath);
			return 1;
		}
	}

#if defined(__NetBSD__) || defined(__OpenBSD__)
	if (strstr(filename, "usb") != NULL) {
		for (uint8_t addr = 1; addr < USB_MAX_DEVICES; addr++) {
			devinfo.udi_addr = addr;
			if (ioctl(fd, USB_DEVICEINFO, &devinfo) == -1) {
				if (optverb)
					warn("%s: ioctl failed.", filename);
				continue;
			}

			if ((devinfo.udi_vendorNo == vid && opt == SEARCHVID) ||
					(devinfo.udi_productNo == pid && opt == SEARCHPID) ||
					(devinfo.udi_vendorNo == vid && devinfo.udi_productNo == pid && opt == (SEARCHVID | SEARCHPID))) {
				close(fd);
				free(filepath);
				return 1;
			}
		}
	}
#endif

	close(fd);
	free(filepath);
	return 0;
}

int
checkstrings(char *filename, const char *vendorreg, const char *productreg, const char *serialreg, int optcase)
{
	int fd;
	char *filepath;
	struct usb_device_info devinfo;
	int goal = 0;
	int ret = 0;

	if ((filepath = (char *) malloc(strlen(filename)+1+5)) == NULL) {
		err(EXIT_FAILURE, "Memory allocation error");
	}

	snprintf(filepath, strlen(filename)+1+5, "/dev/%s", filename);

	if ((fd = open(filepath, O_RDONLY)) == -1) {
		if (optverb)
			warn("%s: open failed.", filename);
		if (errno == EACCES)
			warn("Opening failed.");
		free(filepath);
		return 0;
	}

	if (strstr(filename, "ugen") != NULL) {
		if (ioctl(fd, USB_GET_DEVICEINFO, &devinfo) == -1) {
			if (optverb)
				warn("%s: ioctl failed.", filename);
			close(fd);
			free(filepath);
			return 0;
		}

		if (vendorreg) {
			goal++;
			ret += match_regex(devinfo.udi_vendor, vendorreg, optcase);
		}

		if (productreg) {
			goal++;
			ret += match_regex(devinfo.udi_product, productreg, optcase);
		}

		if (serialreg) {
			goal++;
			ret += match_regex(devinfo.udi_serial, serialreg, optcase);
		}

		if (ret == goal) {
			close(fd);
			free(filepath);
			return 1;
		}
	}

#if defined(__NetBSD__) || defined(__OpenBSD__)
	if (strstr(filename, "usb") != NULL) {
		for (uint8_t addr = 1; addr < USB_MAX_DEVICES; addr++) {

			ret = 0;
			goal = 0;
			devinfo.udi_addr = addr;

			if (ioctl(fd, USB_DEVICEINFO, &devinfo) == -1) {
				if (optverb)
					warn("%s: ioctl failed.", filename);
				continue;
			}

			if (vendorreg) {
				goal++;
				ret += match_regex(devinfo.udi_vendor, vendorreg, optcase);
			}

			if (productreg) {
				goal++;
				ret += match_regex(devinfo.udi_product, productreg, optcase);
			}

			if (serialreg) {
				goal++;
				ret += match_regex(devinfo.udi_serial, serialreg, optcase);
			}

			if (ret == goal) {
				close(fd);
				free(filepath);
				return 1;
			}
		}
	}
#endif

	close(fd);
	free(filepath);
	return 0;
}

void printinfo(struct usb_device_info *devinfo)
{

    printf("IDs      : %04x:%04x\n", devinfo->udi_vendorNo, devinfo->udi_productNo);
    printf("Vendor   : %s\n", devinfo->udi_vendor);
    printf("Product  : %s\n", devinfo->udi_product);
    printf("Serial   : %s\n", devinfo->udi_serial);
    printf("Bus      : %u\n", devinfo->udi_bus);
    printf("Address  : %u\n", devinfo->udi_addr);
    printf("Class    : %u\n", devinfo->udi_class);
    printf("Sudclass : %u\n", devinfo->udi_subclass);
    printf("Power    : %d mA\n", devinfo->udi_power);
    printf("Speed    : %u ", devinfo->udi_speed);
    switch(devinfo->udi_speed) {
    case USB_SPEED_LOW: printf("(Low Speed - 1.5 Mbps)"); break;
    case USB_SPEED_FULL:    printf("(Full Speed - 12 Mbps)"); break;
    case USB_SPEED_HIGH:    printf("(High Speed - 480 Mbps)"); break;
    case USB_SPEED_SUPER:   printf("(Super Speed - 5 Gbps)"); break;
#if defined(__NetBSD__)
    case USB_SPEED_SUPER_PLUS:  printf("(Super Speed+ - 10 Gbps)"); break;
#endif
    default: printf("(unknown speed)");
    }
#if defined(__NetBSD__) || defined(__OpenBSD__)
	printf("\nDriver(s): ");
	for (int i = 0; i < USB_MAX_DEVNAMES; i++)
		if (devinfo->udi_devnames[i][0] != '\0')
			printf("%s ", devinfo->udi_devnames[i]);
#endif
    printf("\n\n");
}

void printall(char *filename, int human)
{
	int fd;
	char *filepath;
	struct usb_device_info devinfo;

	if ((filepath = (char *) malloc(strlen(filename)+1+5)) == NULL) {
		err(EXIT_FAILURE, "Memory allocation error");
	}

	snprintf(filepath, strlen(filename)+1+5, "/dev/%s", filename);

	if ((fd = open(filepath, O_RDONLY)) == -1) {
		if (optverb)
			warn("%s: open failed.", filename);
		if (errno == EACCES)
			warn("Opening failed.");
		free(filepath);
		return;
	}

	if (strstr(filename, "ugen") != NULL) {
		if (ioctl(fd, USB_GET_DEVICEINFO, &devinfo) == -1) {
			if (optverb)
				warn("%s: ioctl failed.", filename);
			close(fd);
			free(filepath);
			return;
		}

		if (human) {
			printf("%s:\n", filepath);
			printinfo(&devinfo);
		} else {
			printf("%s:%04x:%04x:%s:%s:%s:%u:%u:%u:%u:%d:%u\n",
					filepath, devinfo.udi_vendorNo, devinfo.udi_productNo,
					devinfo.udi_vendor, devinfo.udi_product,
					devinfo.udi_serial, devinfo.udi_bus,
					devinfo.udi_addr, devinfo.udi_class,
					devinfo.udi_subclass, devinfo.udi_power,
					devinfo.udi_speed);
		}
	}

#if defined(__NetBSD__) || defined(__OpenBSD__)
	if (strstr(filename, "usb") != NULL) {
		for (uint8_t addr = 1; addr < USB_MAX_DEVICES; addr++) {
			devinfo.udi_addr = addr;
			if (ioctl(fd, USB_DEVICEINFO, &devinfo) == -1) {
				if (optverb)
					warn("%s: ioctl failed.", filename);
				continue;
			}

			if (human) {
				printf("%s:\n", filepath);
				printinfo(&devinfo);
			} else {
				printf("%s:%04x:%04x:%s:%s:%s:%u:%u:%u:%u:%d:%u\n",
						filepath, devinfo.udi_vendorNo, devinfo.udi_productNo,
						devinfo.udi_vendor, devinfo.udi_product,
						devinfo.udi_serial, devinfo.udi_bus,
						devinfo.udi_addr, devinfo.udi_class,
						devinfo.udi_subclass, devinfo.udi_power,
						devinfo.udi_speed);
			}
		}
	}
#endif

	close(fd);
	free(filepath);
}

void
printhelp(char *binname)
{
	printf("ugen USB id finder v0.9\n");
	printf("Copyright (c) 2024 - Denis Bodor\n");
	printf("This tool scans /dev/ugen* for matching device(s) and prints fullpath(s).\n");
    printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -v vendorID      look for this USB Vendor ID (hex)\n");
	printf(" -p productID     look for this USB Product ID (hex)\n");
	printf(" -V vendorName    look for this vendor string (regex)\n");
	printf(" -P productName   look for this product string (regex)\n");
	printf(" -s serial        look for this serial string (regex)\n");
	printf(" -c               use case sensitive regex (default is case insensitive)\n");
	printf(" -l               list all available ugen devices info\n");
	printf(" -a               list all available ugen devices info in pretty form (implies -l)\n");
	printf(" -u               scan /dev/usb* too, not only /dev/ugen* (NetBSD & OpenBSD)\n");
	printf(" -U               scan only /dev/usb* (NetBSD & OpenBSD, implies -u)\n");
	printf(" -d               be verbose (many errors will be displayed). EACCES errors are allways reported\n");
	printf(" -h               show this help\n");
}

uint16_t
hexid(char *arg, char *txt)
{
	uint32_t id;
	char *endptr;

	if(!strlen(arg)) {
		fprintf(stderr, "Invalid %s\n", txt);
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < strlen(arg); i++) {
		if(!isxdigit(arg[i])) {
			fprintf(stderr, "Invalid %s\n", txt);
			exit(EXIT_FAILURE);
		}
	}

	id = strtol(optarg, &endptr, 16);
	if (endptr == optarg) {
		fprintf(stderr, "Invalid %s\n", txt);
		exit(EXIT_FAILURE);
	}

	if (id > 0xffff) {
		fprintf(stderr, "Invalid %s\n", txt);
		exit(EXIT_FAILURE);
	}

	return (uint16_t)(id & 0xffff);
}

int
main (int argc, char**argv)
{
	int retopt;
	int opt = 0;
	int optidscan = 0;
	int optcase = REG_ICASE;
	int optlist = 0;
	int opthuman = 0;
	uint16_t vid;
	uint16_t pid;
	const char *vendorreg = NULL;
	const char *productreg = NULL;
	const char *serialreg = NULL;

	DIR *dir;
	struct dirent *ent;

	while ((retopt = getopt(argc, argv, "v:p:V:P:s:clauUdh")) != -1) {
		switch (retopt) {
		case 'v':
			vid = hexid(optarg, "Vendor ID");
			optidscan |= SEARCHVID;
			opt++;
			break;
		case 'p':
			pid = hexid(optarg, "Product ID");
			optidscan |= SEARCHPID;
			opt++;
			break;
		case 'V':
			vendorreg = strdup(optarg);
			opt++;
			break;
		case 'P':
			productreg = strdup(optarg);
			opt++;
			break;
		case 's':
			serialreg = strdup(optarg);
			opt++;
			break;
		case 'c':
			optcase = 0;
			break;
		case 'l':
			optlist = 1;
			opt++;
			break;
		case 'a':
			optlist = 1;
			opthuman = 1;
			opt++;
			break;
		case 'u':
			optdevusb = 1;
			break;
		case 'U':
			optdevusb = 1;
			optdevusbonly = 1;
			break;
		case 'd':
			optverb = 1;
			break;
		case 'h':
			printhelp(argv[0]);
			return EXIT_SUCCESS;
			break;
		default:
			printhelp(argv[0]);
			return EXIT_FAILURE;
		}
	}

	if (!opt) {
		printhelp(argv[0]);
		return EXIT_FAILURE;
	}

	if (optidscan && (vendorreg || productreg || serialreg)) {
		fprintf(stderr, "Cannot scan for IDs and string at the same time.\n");
		return EXIT_FAILURE;
	}

	if (optlist && (optidscan || vendorreg || productreg || serialreg)) {
		fprintf(stderr, "Cannot scan and list at the same time.\n");
		return EXIT_FAILURE;
	}

#if __OpenBSD__
	if (unveil("/dev", "r") != 0) {
		err(EXIT_FAILURE, "Unveil error:");
	}
#endif

	if ((dir = opendir("/dev")) != NULL) {
		while ((ent = readdir(dir)) != NULL) {
#if defined(__NetBSD__) || defined(__OpenBSD__)
			if ((strstr(ent->d_name, "ugen") != NULL && !optdevusbonly)|| (strstr(ent->d_name, "usb") != NULL && optdevusb)) {
#else
			if (strstr(ent->d_name, "ugen") != NULL) {
#endif
				if (optidscan) {
					if (checkid(ent->d_name, vid, pid, optidscan))
						printf("/dev/%s\n", ent->d_name);
				} else if (vendorreg || productreg || serialreg) {
					if (checkstrings(ent->d_name, vendorreg, productreg, serialreg, optcase))
						printf("/dev/%s\n", ent->d_name);
				} else if (optlist) {
					printall(ent->d_name, opthuman);
				}
			}
		}
		closedir(dir);
	} else {
		if (!optverb)
			return EXIT_FAILURE;
		err(EXIT_FAILURE, "Opendir Error.");
	}

#if __OpenBSD__
	if (unveil(NULL, NULL) != 0) {
		err(EXIT_FAILURE, "Unveil error:");
	}
#endif

	return EXIT_SUCCESS;
}
