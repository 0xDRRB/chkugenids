# chkugenids & findugendev

## chkugenids

A little tool to get information about USB devices from `/dev/ugenN.EE` (endpoint *EE* of device *N*) on OpenBSD, FreeBSD and NetBSD.

The main goal of this program is to display information about USB generic device so you can use them with `devpubd` (NetBSD) or `hotplugd` (OpenBSD) to setup symlinks, permissions, and so on. This may also be interesting for FreeBSD, even if `devd` does the job.

The *ugen* driver provides support for all USB devices that do not have a special driver, like USB NFC Readers, and when no other driver attaches to a device. `chkugenids` can be used in `devpubd`/`hotplugd` scripts to check for VID:PID, vendor name, product name, serial number... and act accordingly.

Exemple: Here we have an ASK LoGO NFC reader (usable with LibNFC) :

```
$ sudo chkugenids -f /dev/ugen1.00 -a
IDs     : 1fd3:0608
Vendor  : ASK
Product : LoGO
Serial  :
Bus     : 3
Address : 2
Class   : 0
Sudclass: 0
Power   : 300 mA
Speed   : 2 (Full Speed - 12 Mbps)
```

We can quickly get VID and PID :

```
$ sudo chkugenids -f /dev/ugen1.00 -i
1fd3:0608
```

or all information on one line (to use with `cut` and `grep`) :

```
$ sudo chkugenids -f /dev/ugen1.00 -l
1fd3:0608:ASK:LoGO::3:2:0:0:300:2
```

Let's say we want users from group `wheel` to be able to read and write this device without `sudo` (with `nfc-list` or `nfc-poll`). On NetBSD, just add `devpubd=YES` in your `/etc/rc.conf` and create a script in `/libexec/devpubd-hooks`. Something like a `04-NFCsetperm` containing :

```bash
#!/bin/sh
#
# Change permissions on /dev/ugenN.EE
#

event="$1"
shift
devices=$@

for device in $devices; do
   case $device in
   ugen*)
      case $event in
      device-attach)
         echo "NFCsetperm: $device attached"
         chkugenids -f /dev/$device.00 -i -q | grep "1fd3:0608" > /dev/null
         if [ $? -eq 0 ]; then
            logger -s "NFCsetperm: device match. Adjusting permissions for /dev/$device.*"
            chmod g+rw /dev/$device.*
         fi
         ;;
      device-detach)
         logger -s "NFCsetperm: $device detached. Setting back permissions on /dev/$device.*!"
         chmod 600 /dev/$device.*
         ;;
      esac
      ;;
   esac
done
```

Or even better, with a list of IDs, without `grep` :

```bash
#!/bin/sh
#
# Change permissions on /dev/ugenN.EE
#

event="$1"
shift
devices=$@

# list of VIP:PID for NFC readers
IDS="
04cc:0531
054c:0193
04cc:2533
072f:2200
1fd3:0608
04e6:5591
"

CHKUGENIDS=/path/to/chkugenids

for device in $devices; do
   case $device in
   ugen*)
      case $event in
      device-attach)
         echo "NFCsetperm: $device attached"
         DEVID=`$CHKUGENIDS -f /dev/$device.00 -i -q`
         for i in $IDS
         do
            if [ "$DEVID" = "$i" ]
            then
               logger -s "NFCsetperm: device match. Adjusting permissions for /dev/$device.*"
               chmod g+rw /dev/$device.*
            fi
         done
         ;;
      device-detach)
         logger -s "NFCsetperm: $device detached. Setting back permissions on /dev/$device.*"
         chmod 600 /dev/$device.*
         ;;
      esac
      ;;
   esac
done
```

When the reader device is plugged, `/dev/ugenN.EE` is created by `/libexec/devpubd-hooks/01-makedev`, then `devpubd` call `04-NFCsetperm` where `chkugenids` is used to check for VID:PID and set `g+rw` to `/dev/ugenN.*` if we have a match. Of course you can check for matches on anything `chkugenids` can provide (try `chkugenids -h`).


## findugendev
This tool scans `/dev/ugen*` for matching device(s) and prints fullpath(s). The main objective is to find the `/dev` entry corresponding to the USB device you are looking for. The search can be done on USB IDs or on textual device information, with regex, such as vendor name or serial number. The tool can also list all information by browsing all `/dev` entries attached to active devices.

`findugendev` works with FreeBSD, NetBSD and OpenBSD.

Here are some examples of use:

* Find all ugen devices with VendorID 0x2e8a (Raspberry Pi Pico):

```
$ sudo findugendev -v 2e8a
/dev/ugen2.00
```

* Same thing but with several Pico cards connected (in BOOT mode):

```
$ sudo findugendev -v 2e8a
/dev/ugen1.00
/dev/ugen2.00
```

* Same thing but with a regular expression on the device name:

```
$ sudo findugendev -P "B..t"
/dev/ugen1.00
/dev/ugen2.00
```

* Same thing but with a case-sensitive search:

```
$ sudo findugendev -V STM -c
/dev/ugen0.00
```

* Listing ugen devices (on NetBSD):

```
$ sudo findugendev -l
/dev/ugen0.00:0483:2016:STMicroelectronics:Biometric Coprocessor::0:2:0:0:100:2
/dev/ugen2.00:2e8a:0003:Raspberry Pi:RP2 Boot:E0C912952D54:1:2:0:0:500:2
```

* Same thing under FreeBSD (all USB devices have a ugen entry):

```
$ sudo findugendev -l
/dev/ugen0.1:0000:0000:(0x1b21):XHCI root HUB::0:1:9:0:0:4
/dev/ugen1.1:0000:0000:AMD:XHCI root HUB::1:1:9:0:0:4
/dev/ugen2.1:0000:0000:AMD:XHCI root HUB::2:1:9:0:0:4
/dev/ugen0.2:0b95:1790:ASIX Elec. Corp.:AX88179:0000249B7720EB:0:1:255:255:124:4
/dev/ugen1.2:046a:0023:vendor 0x046a:product 0x0023::1:1:0:0:100:1
/dev/ugen1.3:0bda:0129:Generic:USB2.0-CRW:20100201396000000:1:2:255:255:500:3
/dev/ugen2.2:0403:6001:FTDI:FT232R USB UART:A9IPDVNN:2:1:0:0:90:2
/dev/ugen2.3:2109:2817:VIA Labs, Inc.:USB2.0 Hub:000000000:2:2:9:0:0:3
/dev/ugen2.5:2109:0817:VIA Labs, Inc.:USB3.0 Hub:000000000:2:4:9:0:0:4
```

* Same but scans `/dev/usb*` too on OpenBSD :

```
$ sudo ./findugendev -lu
/dev/ugen0.00:0a5c:219b:Broadcom Corp:Broadcom Bluetooth 2.1 Device:506313AE7C7E:3:2:224:1:0:2
/dev/ugen1.00:04e6:5591:SCM Micro:SCL3711-NFC&RW::4:2:0:0:100:2
/dev/ugen2.00:2e8a:0003:Raspberry Pi:RP2 Boot:E0C912952D54:1:3:0:0:500:2
/dev/usb0:8086:0000:Intel:EHCI root hub::0:1:9:0:0:3
/dev/usb0:0ac8:c33f:Namuga.:WebCam SCB-0340N::0:2:239:2:128:3
/dev/usb1:8086:0000:Intel:UHCI root hub::1:1:9:0:0:2
/dev/usb1:0eef:480e:eGalax Inc.:USB TouchController::1:2:0:0:100:2
/dev/usb1:2e8a:0003:Raspberry Pi:RP2 Boot:E0C912952D54:1:3:0:0:500:2
/dev/usb2:8086:0000:Intel:UHCI root hub::2:1:9:0:0:2
/dev/usb2:0403:6001:FTDI:FT232R USB UART:A9016VUS:2:2:0:0:90:2
/dev/usb3:8086:0000:Intel:UHCI root hub::3:1:9:0:0:2
/dev/usb3:0a5c:219b:Broadcom Corp:Broadcom Bluetooth 2.1 Device:506313AE7C7E:3:2:224:1:0:2
/dev/usb4:8086:0000:Intel:UHCI root hub::4:1:9:0:0:2
/dev/usb4:04e6:5591:SCM Micro:SCL3711-NFC&RW::4:2:0:0:100:2
```

* Same thing (on NetBSD) but in a more readable form:

```
$ sudo findugendev -a
/dev/ugen0.00:
IDs     : 0483:2016
Vendor  : STMicroelectronics
Product : Biometric Coprocessor
Serial  :
Bus     : 0
Address : 2
Class   : 0
Sudclass: 0
Power   : 100 mA
Speed   : 2 (Full Speed - 12 Mbps)

/dev/ugen2.00:
IDs     : 2e8a:0003
Vendor  : Raspberry Pi
Product : RP2 Boot
Serial  : E0C912952D54
Bus     : 1
Address : 2
Class   : 0
Sudclass: 0
Power   : 500 mA
Speed   : 2 (Full Speed - 12 Mbps)
```

`findugendev` can be used together with `chkugenids` in `devpubd` (NetBSD) or `hotplugd` (OpenBSD) scripts to identify the entry corresponding to a device and, for example, adjust permissions automatically.

